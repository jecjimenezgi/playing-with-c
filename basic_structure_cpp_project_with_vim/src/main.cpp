#include <iostream>
#include <operation.hpp>

extern "C" 
{ 
#include <imprimir.h>
}

using namespace std;


int main(){
    int x = 2;
    int y = 15;
    int k = 34;
    Operation op(x,y);
    cout << "1:" << op.get_x() << endl;
    cout << "2:" << op.get_y() << endl;
    cout << "3:" << op.get_suma() << endl;
    cout << "4:" << op.get_resta() << endl;
    cout << "5:" << op.get_suma(k,y) << endl;
    cout << "6:" << op.get_div() << endl;
    cout << "7:" << op.get_mod() << endl;
    cout << "8:" << op.get_mod(3,2) << endl;
    imprimir(x);
    return 0;
}
