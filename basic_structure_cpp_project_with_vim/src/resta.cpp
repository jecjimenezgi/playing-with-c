#include <resta.hpp>

Resta::Resta(int x1 , int y1)
{ 
    set_x(x1);
    set_y(y1);
}

int Resta::get_resta()
{
    return get_x() - get_y();
}

int Resta::get_resta(int x1, int y1){
    return x1 - y1;
}
