#include <suma.hpp>

Suma::Suma(int w1, int z1)
{ 
    set_w(w1);
    set_z(z1);
}

int Suma::get_suma()
{ 
    return get_w() + get_z(); 
}

int Suma::get_suma(int x , int y)
{
    return x + y;
} 
