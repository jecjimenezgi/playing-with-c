#include <resta.hpp>
#include <suma.hpp>
#include <extop.hpp>
#include <extso.hpp>

#ifndef OPERATION_H
#define OPERATION_H


class Operation: public Suma, public Resta, public Extop, public Extso
{
    private:
        int x;
        int y;
    public:
        Operation(int x1, int y1);
        void set_x(int x1){ x = x1; }
        void set_y(int y1){ y = y1; }
        int get_x(){ return x; }
        int get_y(){ return y; }

}; 


#endif

