#ifndef SUM_H
#define SUM_H

class Suma
{
    private:
        int w;
        int z;
    protected:
        Suma(int w1, int z1);
        void set_w(int w1){ w = w1; }
        void set_z(int z1){ z = z1; }
        int get_w(){ return w; }
        int get_z(){ return z; }
    public:
        int get_suma();
        int get_suma(int x , int y);
};
 
#endif
