#ifndef RESTA_H
#define RESTA_H

class Resta
{
    private:
        int x;
        int y;
    protected:
       Resta(int x1 , int y1);
       void set_x(int x1){ x = x1;}
       void set_y(int y1){ y = y1;} 
       int get_x(){ return x; }
       int get_y(){ return y; }
    public:
        int get_resta();
        int get_resta(int x, int y);
};
#endif
