#https://github.com/ycm-core/YouCompleteMe/issues/3293

import os,sys
flags = [
# Your default flags
]
for os_walk_entry in os.walk( "./" ):
        if os.path.isdir( os_walk_entry[ 0 ] ):
                flags += [ '-I', os_walk_entry[ 0 ] ]
def Settings( **kwargs ):
        return { 'flags': flags }
