#include <mod.hpp>

Mod::Mod(int x1, int y1)
{ 
    set_x(x1);
    set_y(y1);
}

int Mod::get_mod()
{ 
    return get_x() % get_y(); 
}

int Mod::get_mod(int x , int y)
{
    return x % y;
}
