#include <mod.hpp>

#ifndef EXTSO_H
#define EXTSO_H

class Extso: public Mod
{
    private:
        int x;
        int y;
    public:
        Extso(int x1, int y1);
        void set_x(int x1){ x = x1; }
        void set_y(int y1){ y = y1; }
        int get_x(){ return x; }
        int get_y(){ return y; }

}; 


#endif

