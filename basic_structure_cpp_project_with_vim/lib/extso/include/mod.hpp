#ifndef MOD_H
#define MOD_H

class Mod
{
    private:
        int x;
        int y;
    protected:
        Mod(int x1, int y1);
        void set_x(int x1){ x = x1; }
        void set_y(int y1){ y = y1; }
        int get_x(){ return x; }
        int get_y(){ return y; }
    public:
        int get_mod();
        int get_mod(int x , int y);
};
 
#endif
