#include <div.hpp>

Div::Div(int x1, int y1)
{ 
    set_x(x1);
    set_y(y1);
}

int Div::get_div()
{ 
    return get_x() * get_y(); 
}

int Div::get_div(int x , int y)
{
    return x + y;
}
