#ifndef MUL_H
#define MUL_H

class Mul
{
    private:
        int x;
        int y;
    protected:
        Mul(int x1, int y1);
        void set_x(int x1){ x = x1; }
        void set_y(int y1){ y = y1; }
        int get_x(){ return x; }
        int get_y(){ return y; }
    public:
        int get_mul();
        int get_mul(int x , int y);
};
 
#endif
