#ifndef DIV_H
#define DIV_H

class Div
{
    private:
        int x;
        int y;
    protected:
        Div(int x1, int y1);
        void set_x(int x1){ x = x1; }
        void set_y(int y1){ y = y1; }
        int get_x(){ return x; }
        int get_y(){ return y; }
    public:
        int get_div();
        int get_div(int x , int y);
};
 
#endif
