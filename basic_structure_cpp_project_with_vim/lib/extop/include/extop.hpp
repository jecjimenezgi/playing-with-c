#include <div.hpp>
#include <mul.hpp>

#ifndef EXTOP_H
#define EXTOP_H

class Extop: public Div, public Mul
{
    private:
        int x;
        int y;
    public:
        Extop(int x1, int y1);
        void set_x(int x1){ x = x1; }
        void set_y(int y1){ y = y1; }
        int get_x(){ return x; }
        int get_y(){ return y; }

}; 


#endif

