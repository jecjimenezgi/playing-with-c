#include <stdio.h>
#include <stdlib.h>

typedef struct P P;

typedef struct Q Q; 

typedef struct W W;
 

struct W
{ 
	int count;
}; 


struct P 
{	
	void (*M)(Q *Q_OTHER,P *P_OTHER, W *C);
}; 


struct Q
{
	void (*S)(P *P_OTHER,Q *Q_OTHER, W *C);
};

void f1 (Q *Q_OTHER,P *P_OTHER, W *C )
{ 
	if( C->count < 10 )
	{ 
		C->count += 1;		
		Q_OTHER->S(P_OTHER,Q_OTHER,C); 
	}
	else{
		printf("este es el valor calculado %d \n", C->count);
	}
}

void f2 (P *P_OTHER,Q *Q_OTHER, W *C )
{
	if ( C->count < 10 )
	{
	       C->count += 1;
	       P_OTHER->M(Q_OTHER,P_OTHER,C);	       
	}
	else
	{
		printf("este es el valor calculado %d \n", C->count);
	}
}

int main()
{       
	W *w;
	P *p;
	Q *q;

	w = malloc(sizeof(struct W));
	p = malloc(sizeof(struct P));
	q = malloc(sizeof(struct Q));


       	w->count = 0;	
 	p->M = f1;
	q->S = f2;

	p->M(q,p,w);

	return 0;
}
